const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const path = require("path")
const {ticket} = require('./router/ticket')
const cors = require('cors')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(express.static(path.join(__dirname, 'public')))

app.use('/ticket', ticket)

app.use(cors())

app.get('/', (req,res) => {
    res.send('welcome')
})

app.listen( 3000,() => console.log('server running at http://localhost:3000'))
