const express = require("express");
const router = express.Router();
const process = require('process')
const con = require('../services/dbConnection')
const getElementById = require('../services/dbCrud')
// Home page route.

const ticketPostHandler = async (req,res) => {
    const {userId , doctorId} = req.body
    if( !Number.isInteger(parseInt(userId)) || !Number.isInteger(parseInt(doctorId))){
        res.status(400).send('Bad Request (please send integer data)')
        return
    }
    let row = await getElementById('doctors',doctorId)
    let doctor = row[0]
    if (!doctor){
        res.status(404).send('doctor not found')
        return
    }
    if(JSON.parse(doctor.ticketList).includes(parseInt(userId))){
        res.status(421).send('You have a ticket from this doctor and you cannot get a ticket again')
        return
    }
    if(doctor.ticketCount >= doctor.maxTicketCount){
        res.status(450).send("The doctor's tickets have been filled")
        return
    }
            
    let sql = `UPDATE doctors set ticketCount = ticketCount + 1 , ticketList = JSON_ARRAY_APPEND(ticketList, '$',${userId}) WHERE id= ${doctorId}`
    
    con.query(sql, (err, result) => {
            if (err) throw err;
            console.log(result.affectedRows + " record(s) updated");
        });
    res.status(201).send('you have successfully received a ticket')
    res.end()
}

router.get('/', (req, res) => {
    res.sendFile(process.cwd() + '/public/getTicket.html');
})

router.post('/', ticketPostHandler)






module.exports = {ticket: router , ticketPostHandler}