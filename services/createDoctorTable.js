
const con = require('./dbConnection')


con.connect((err) => {
  if (err) throw err;
  console.log("Connected!");
  let sql = "CREATE TABLE doctors(id INT PRIMARY KEY, ticketCount INT,maxTicketCount INT,ticketList JSON)"
  con.query(sql, (err, result) => {
    if (err) throw err;
    console.log("Table created");
    con.end()
  });
});
