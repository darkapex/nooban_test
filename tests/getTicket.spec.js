const request = require('supertest')
const express = require('express')
const app = express()
const bodyParser = require('body-parser')

const {ticket} = require('../router/ticket')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/', ticket);

describe("Test Handlers", () => {
    test('GET / (render credential field)', (done) => {
        request(app)
        .get("/")
        .expect(200)
        .end((err,res) => {
            if (err) return done(err);
            return done()
        })
    })
    test('POST /ticket (getTicket)', (done) => {
        const data = {
            userId: 123,
            doctorId: 3
        }
        request(app)
        .post("/")
        .expect(201)
        .send(data)
        .end((err,res) => {
            if (err) return done(err);
            return done()
        })
    })
  });